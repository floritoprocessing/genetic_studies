import java.net.URISyntaxException;
import java.util.Collections;

import graf.genetics.Population;
import processing.core.PApplet;
import processing.core.PFont;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class PathfindingTest extends PApplet {
	
	private static final int CYCLES = 200;
	
	Obstacle goal = new Obstacle(725,300,20,20);
	
	Obstacle[] obstacles = new Obstacle[] {
			new Obstacle(200,300,20,400),
			new Obstacle(400,100,20,200),
			new Obstacle(400,500,20,200),
			new Obstacle(600,300,20,400),
			goal
	};
	
	
	
	Population<Punky> population = new Population<Punky>();
	
	PFont font;
	int cycle = 0;
	
	public void settings() {
		size(800,600,JAVA2D);
	}
	
	public void setup() {
		
		smooth();
//		try {
			String path = "Calibri-Bold-12.vlw";
			font = loadFont(path);
			textFont(font);
//		} catch (URISyntaxException e) {
//			e.printStackTrace();
//		}
		
		Punky[] punkies = new Punky[100];
		for (int i=0;i<punkies.length;i++) {
			punkies[i] = new Punky(75,300);
		}
		population.init(punkies);
		population.registerGene("color");
		population.registerGene("xvec");
		population.registerGene("yvec");
		System.out.println("nr of genes: "+punkies[0].dna.getNumberOfGenes());
		System.out.print("lengths of genes in bytes: ");
		int[] lengths = punkies[0].dna.getSizeOfGenes();
		for (int i=0;i<lengths.length;i++) {
			System.out.print(lengths[i]+" ");
		}
		System.out.println();
	}
	
	public void draw() {
		if (cycle==0 && frameCount!=1) {
			delay(2000);
		}
		
		background(240);
		drawObstacles();
		drawPunkies();
		for (Punky p:population.individuals()) {
			p.move(obstacles);
		}
		cycle++;
		
		fill(0);
		text("Pathfinding",10,20);
		text("Cycle: "+cycle,10,34);
		
		
		if (cycle==CYCLES) {
			text("Generating next generation",10,48);
			
			/*
			 * Evaluate fitness;
			 */
			float maxDistance = dist(0, 0, goal.x, goal.y);
			float distance, distancePercentage;
			for (Punky p:population.individuals()) {
				distance = dist(p.x, p.y, goal.x, goal.y);
				distancePercentage = distance/maxDistance;
				p.fitness = 1 - distancePercentage;
			}
			
			/*
			 * Sort on fitness
			 */
			Collections.sort(population.individuals());
			text("Fittest: "+population.individuals().get(population.individuals().size()-1).fitness, 10, 62);
			
			/*
			 * Crossing-over
			 */
			population.breedNextGeneration();
			
			cycle=0;
		}
	}
	
	void drawObstacles() {
		stroke(0);
		fill(128);
		rectMode(CENTER);
		rect(75,300,20,20);
		for (Obstacle o:obstacles) {
			o.draw(this);
		}
	}

	void drawPunkies() {
		stroke(0);
		fill(192);
		ellipseMode(CENTER);
		for (Punky p:population.individuals()) {
			ellipse(p.x, p.y, 10, 10);
		}
	}
	
	
	
	
	public static void main(String[] args) {
		PApplet.main(new String[] {"PathfindingTest"});
	}
	
	
}
