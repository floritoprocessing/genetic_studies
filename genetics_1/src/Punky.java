import processing.core.PVector;
import graf.genetics.Creature;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class Punky extends Creature {

	private static final int gridWidth = 800;
	private static final int gridHeight = 600;
	private static final float gridSize = 40;
	
	int color;
	byte[] xvec;
	byte[] yvec;
	
	boolean dead = false;
	float x, y;
	PVector mov = new PVector();
	
	public Punky(float x, float y) {
		this.x = x;
		this.y = y;
		color = (int)(Math.random()*0xffffff);
		int lowwidth = (int)(gridWidth/gridSize);
		int lowheight = (int)(gridHeight/gridSize);
		xvec = new byte[lowwidth*lowheight];
		yvec = new byte[lowwidth*lowheight];
		
		for (int i=0;i<xvec.length;i++) {
			float speed = 255;
			float dir = (float)(Math.random()*Math.PI*2);
			xvec[i] = (byte)(speed*Math.cos(dir));
			yvec[i] = (byte)(speed*Math.sin(dir));
			//System.out.println(xvec[i]+"/"+yvec[i]);
		}
	}
	
	public void move(Obstacle[] obstacles) {
		if (!dead) {
			int ix = (int)(x/gridSize);
			int iy = (int)(y/gridSize);
			int i = iy * ((int)(gridWidth/gridSize)) + ix;
			float accX = xvec[i] / 128.0f;
			float accY = yvec[i] / 128.0f;
			mov.x += accX;
			mov.y += accY;
			float speed = mov.mag();
			if (speed>5) {
				mov.mult(5/speed);
			}
			x += mov.x;
			y += mov.y;
			if (x<0 || x>=gridWidth || y<0 || y>=gridHeight) {
				dead = true;
			}
			for (Obstacle o:obstacles) {
				if (o.intersects(x, y)) {
					dead = true;
				}
			}
		}
	}
	
}
