import java.awt.Rectangle;

import processing.core.PApplet;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class Obstacle {
	
	int x, y;
	int w, h;
	Rectangle rect;
	
	public Obstacle(int x, int y, int w, int h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		
		rect = new Rectangle(w, h);
		rect.setLocation(x-w/2, y-h/2);
	}
	
	public void draw(PApplet pa) {
		pa.rect(x,y,w,h);
	}
	
	public boolean intersects(float x, float y) {
		return rect.contains(x, y);
	}
}
