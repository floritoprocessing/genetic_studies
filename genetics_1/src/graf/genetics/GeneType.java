package graf.genetics;
/**
 * 
 */

/**
 * @author Marcus
 *
 */
public enum GeneType {

	Byte,
	Int,
	Float,
	
	ByteArray,
	IntArray,
	FloatArray,
	
}
