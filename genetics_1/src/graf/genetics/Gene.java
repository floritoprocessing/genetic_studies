package graf.genetics;

import java.lang.reflect.Field;

/**
 * @author Marcus
 *
 */
public class Gene {// implements Serializable {
	
	GeneType type;
	byte[] raw;

	public Gene(Field field) {
		
		Class<?> fieldType = field.getType();
		Class<?> componentType = fieldType.getComponentType();
		
		if (fieldType.isArray()) {
			if (componentType.equals(byte.class)){
				type = GeneType.ByteArray;
			}
			else if (componentType.equals(int.class)){
				type = GeneType.IntArray;
			}
			else if (componentType.equals(float.class)){
				type = GeneType.FloatArray;
			}			
		} else {
			if (fieldType.equals(byte.class)){
				type = GeneType.Byte;
			}
			else if (fieldType.equals(int.class)){
				type = GeneType.Int;
			}
			else if (fieldType.equals(float.class)){
				type = GeneType.Float;
			}
		}
		
	}

	public void makeRawData(GeneMap<? extends Creature> geneMap, Creature creature) {
		Field field = geneMap.getFieldForGene(this);
		field.setAccessible(true);
		Object value = null;
		try {
			value = field.get(creature);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		if (value==null) {
			throw new RuntimeException("error");
		} else {
			switch (type) {
			case Byte:
				raw = RawConversion.toByta((Byte)value);
				break;
			case Int:
				raw = RawConversion.toByta((Integer)value);
				break;
			case Float:
				raw = RawConversion.toByta((Float)value);
				break;
			case ByteArray:
				raw = RawConversion.toByta((byte[])value);
				break;
			case IntArray:
				raw = RawConversion.toByta((int[])value);
				break;
			case FloatArray:
				raw = RawConversion.toByta((float[])value);
				break;
				
			default:
				break;
			}
		}
	}
	
	 /*private void writeObject(java.io.ObjectOutputStream out) throws IOException {
	 }
	 private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
	 }*/

}
