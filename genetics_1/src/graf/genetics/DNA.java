package graf.genetics;

import java.util.ArrayList;

/**
 * 
 */

/**
 * @author Marcus
 * 
 */
public class DNA {

	ArrayList<Gene> genes = new ArrayList<Gene>();

	public DNA() {
	}

	public void addGene(Gene gene) {
		genes.add(gene);
	}

	/**
	 * @return
	 */
	public int getNumberOfGenes() {
		return genes.size();
	}

	/**
	 * @return
	 */
	public int[] getSizeOfGenes() {
		int[] out = new int[genes.size()];
		for (int i=0;i<out.length;i++) {
			out[i] = genes.get(i).raw.length;
		}
		return out;
	}
	


}
