package graf.genetics;

import java.lang.reflect.Field;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public abstract class Creature implements Comparable<Creature> {

	public DNA dna = new DNA();
	public float fitness;
		
	

	@Override
	public int compareTo(Creature o) {
		if (fitness<o.fitness) {
			return -1;
		} else if (fitness>o.fitness) {
			return 1;
		}
		return 0;
	}
	
	
}
