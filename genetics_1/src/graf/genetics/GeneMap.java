/**
 * 
 */
package graf.genetics;

import java.lang.reflect.Field;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * @author Marcus
 *
 */
public class GeneMap<E extends Creature> {

	Hashtable<Field, Gene> map = new Hashtable<Field, Gene>();
	
	public GeneMap() {
	}
	
	public Field registerGene(E creature, String fieldName) {
		Field[] fields = creature.getClass().getDeclaredFields();
		for (Field field:fields) {
			if (field.getName().equals(fieldName)) {
				
				Gene gene = new Gene(field);
				map.put(field, gene);
				
				return field;
			}
		}
		
		throw new RuntimeException("Field named "+fieldName+" not found");
	}

	/**
	 * @param field
	 * @return
	 */
	public Gene getGeneForField(Field field) {
		return map.get(field);
	}

	/**
	 * @param gene
	 * @return
	 */
	public Field getFieldForGene(Gene gene) {
		Enumeration<Field> fields = map.keys();
		while (fields.hasMoreElements()) {
			Field field = fields.nextElement();
			if (map.get(field).equals(gene)) {
				return field;
			}
		}
		throw new RuntimeException("Gene "+gene+" not found in GeneMap");
	}
}
