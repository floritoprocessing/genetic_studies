/**
 * 
 */
package graf.genetics;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Marcus
 *
 */
public class Population<E extends Creature> {
	
	public GeneMap<E> geneMap = new GeneMap<E>();
	
	ArrayList<E> creatures = new ArrayList<E>();
	
	public Population() {
		
	}
	
	public ArrayList<E> individuals() {
		return creatures;
	}
	
	public void init(E[] creatureArray) {
		creatures.addAll(Arrays.asList(creatureArray));
	}
	
	public void registerGene(String fieldName) {
		Field field = geneMap.registerGene(creatures.get(0), fieldName);
		Gene gene = geneMap.getGeneForField(field);
		for (E creature:creatures) {
			creature.dna.addGene(gene);
			gene.makeRawData(geneMap,creature);
		}
	}

	public void breedNextGeneration() {
		// TODO Auto-generated method stub
		
	}
	
}
