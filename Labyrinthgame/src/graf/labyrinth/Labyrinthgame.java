package graf.labyrinth;

//import graf.labyrinth.Labyrinth;
import java.util.Vector;

import processing.core.*;

public class Labyrinthgame extends PApplet{

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.labyrinth.Labyrinthgame"});
	}
	
	Labyrinth labyrinth;
	Vector bugs = new Vector();
	int frameModDraw = 2;
	
	public void settings() {
		size(800,600,P3D);
	}
	
	public void setup() {
		
		labyrinth = new Labyrinth(width,height,40);
		//for (int i=0;i<20;i++) labyrinth.addBlock((int)random(width), (int)random(height));
		background(0);
		frameRate(1000);
	}
	
	public void mousePressed() {
		if (mouseButton==LEFT)
			labyrinth.toggleBlock(mouseX,mouseY);
		else if (mouseButton==RIGHT)
			labyrinth.newEndPosition();
	}
	
	public void draw() {
		
		if (bugs.size()<labyrinth.MAX_BUGS/5) labyrinth.createBug(bugs); //else init=false;		
		for (int i=0;i<bugs.size();i++) {
			Bug b = ((Bug)bugs.elementAt(i));
			if (!b.move(labyrinth)) {
				bugs.remove(b);
				i--;
			}
		}
		labyrinth.marryBugs(bugs);
		
		
		if (frameCount%frameModDraw==0) {
			background(0);
			labyrinth.draw(this);
			for (int i=0;i<bugs.size();i++) ((Bug)bugs.elementAt(i)).draw(this);
		}
		
		//System.out.println(bugs.size());
	}
}
