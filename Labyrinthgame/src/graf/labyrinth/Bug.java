package graf.labyrinth;

import processing.core.*;

public class Bug {
	
	private int size = 4;
	public float x, y;
	private float xm, ym;
	private boolean running = true;
	private float life = 2000;
	private float maxLife = 2000;
	
	private static final float MIN_SPEED = 1f;
	
	private float dir = (float)(Math.random()*Math.PI*2);
	private float startDir = dir;
	private float speed = (float)(MIN_SPEED+6*Math.random());
	private float minDirChange = (float)(0.1*Math.random());
	private float maxDirChange = (float)(Math.random());
	private float dirChangeFrequency = (float)Math.random();
	public int nrOfChildren = (int)(1+Math.random()*10);
	
	public Bug(int screenX, int screenY) {
		x = screenX;
		y = screenY;
		startDir = dir;
		makeMovVec();
	}
	

	private void setDirection(float f) {
		dir = f;
		startDir = dir;
		makeMovVec();
	}
	
	public String toString() {
		String out = "";
		out+="speed: "+speed;
		out+="dir: "+dir;
		out+="\tminDirChange: "+minDirChange;
		out+="\tmaxDirChange: "+maxDirChange;
		out+="\tdirChangeFrequency: "+dirChangeFrequency;
		out+="\tnrOfChildren: "+nrOfChildren;
		return out;
	}
	
	private void makeMovVec() {
		xm = speed * (float)Math.cos(dir);
		ym = speed * (float)Math.sin(dir);
	}

	private void makeDir() {
		dir = (float)Math.atan2(ym,xm);
	}
	
	/**
	 * Moves the Bug and returns if it still alive
	 * @param labyrinth
	 * @return
	 */
	public boolean move(Labyrinth labyrinth) {
		if (!running) return true;
		
		if (Math.random()<dirChangeFrequency) {
			dir += (float)(minDirChange+Math.random()*(maxDirChange-minDirChange)) * (Math.random()<0.5?-1:1);
			makeMovVec();
		}
		
		
		x+=xm;
		y+=ym;
		
		if (x<0&&xm<0) {
			x=0;
			xm=-xm;
			makeDir();
		}
		if (x>labyrinth.screenWidth()&&xm>0) {
			x=labyrinth.screenWidth();
			xm=-xm;
			makeDir();
		}
		if (y<0&&ym<0) {
			y=0;
			ym=-ym;
			makeDir();
		}
		if (y>labyrinth.screenHeight()&&ym>0) {
			y=labyrinth.screenHeight();
			ym=-ym;
			makeDir();
		}
		
		for (int i=0;i<labyrinth.block.length;i++)
			for (int j=0;j<labyrinth.block[0].length;j++)
				if (labyrinth.block[i][j]!=null) checkBorder(labyrinth.block[i][j]);
		
		life-=speed;
		return (life>0);
	}

	private void checkBorder(Block block) {
		float fx=x+xm;
		float fy=y+ym;
		int left=block.left, right=block.right, top=block.top, bottom=block.bottom;
		// horizontal bounce:
		if (block.verticalIn(y)) {
			if (x<=left&&fx>left) { x=left; xm=-xm; makeDir(); }
			else if (x>=right&&fx<right) { x=right; xm=-xm; makeDir(); }
		}
		// vertical bounce:
		if (block.horizontalIn(x)) {
			if (y<=top&&fy>top) { y=top; ym=-ym; makeDir(); }
			else if (y>=bottom&&fy<bottom) { y=bottom; ym=-ym; makeDir(); }
		}
	}

	public void draw(PApplet pa) {
		pa.rectMode(PApplet.CORNER);
		pa.fill(255,0,0);
		pa.stroke(255);
		float s = size * (float)life/maxLife;
		pa.rect(x-s/2,y-s/2,s,s);
	}

	public void stop() {
		running=false;
	}

	private static float rnd() {
		float momDad = (float)(Math.random()<0.5?0:1);
		// one in 20 is morph
		// if (Math.random()<0.05) return (float)Math.random();
		
		// slight morph
		momDad += (float)(0.1-Math.random()*0.2);
		return momDad;
	}
	
	public static Bug createChild(Block block, Bug b1, Bug b2) {
		int x = block.cenX();
		int y = block.cenY();
		Bug out = new Bug(x,y);
		float xm1 = (float)Math.cos(b1.startDir);
		float ym1 = (float)Math.sin(b1.startDir);
		float xm2 = (float)Math.cos(b2.startDir);
		float ym2 = (float)Math.sin(b2.startDir);
		//float rnd = (float)(Math.random()<0.5?0:1);	// FULL CLONE OF ONE OF THE PARENTS
		float r=rnd();
		float aym = ym1 + (float)(r*(ym2-ym1));
		float axm = xm1 + (float)(r*(xm2-xm1));
		float dir = (float)Math.atan2(aym,axm);
		out.setDirection(dir);
		out.speed = b1.speed + (float)(rnd()*(b2.speed-b1.speed));
		if (out.speed<MIN_SPEED) out.speed=MIN_SPEED;
		out.minDirChange = b1.minDirChange + (float)(rnd()*(b2.minDirChange-b1.minDirChange));
		out.maxDirChange = b1.maxDirChange + (float)(rnd()*(b2.maxDirChange-b1.maxDirChange));
		out.dirChangeFrequency = b1.dirChangeFrequency + (float)(rnd()*(b2.dirChangeFrequency-b1.dirChangeFrequency));
		out.nrOfChildren = (int)(b1.nrOfChildren + (rnd()*(b2.nrOfChildren-b1.nrOfChildren)));
		if (out.nrOfChildren<1) out.nrOfChildren=1;
		return out;
	}

	
}
