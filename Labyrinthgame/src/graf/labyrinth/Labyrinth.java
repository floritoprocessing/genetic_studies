package graf.labyrinth;

import java.util.Vector;

import processing.core.PApplet;

public class Labyrinth {

	private int screenWidth, screenHeight;
	private int width, height;
	private int blockSize = 1;
	private float screenToBlock;
	
	public int MAX_BUGS = 3000;
	
	Block[][] block;
	
	Block start, end;
	Vector inEnd = new Vector();
	
	public Labyrinth(int screenWidth, int screenHeight, int blockSize) {
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
		this.blockSize = blockSize;
		screenToBlock = 1.0f/blockSize;
		width = sI(screenWidth);
		height = sI(screenHeight);
		block = new Block[width][height];
		start = new Block(blockSize,width/2,height/2,0x0000FF);
		newEndPosition();
	}
	
	public void newEndPosition() {
		end = new Block(blockSize,(int)(Math.random()*width),(int)(Math.random()*height),0x00FF00);
	}
	
	public int screenWidth() {
		return screenWidth;
	}
	
	public int screenHeight() {
		return screenHeight;
	}
	
	private int sI(int sp) {
		return (int)(sp*screenToBlock);
	}

	public void addBlock(int mouseX, int mouseY) {
		int i=sI(mouseX), j=sI(mouseY);
		Block addBlock = new Block(blockSize,i,j);
		if (!addBlock.onTopOf(start)&&!addBlock.onTopOf(end)) {
			block[i][j] = addBlock;
		}
	}
	
	public void toggleBlock(int mouseX, int mouseY) {
		int i=sI(mouseX), j=sI(mouseY); 
		if (block[i][j]==null)
			addBlock(mouseX,mouseY);
		else {
			block[i][j]=null;
		}
	}
	
	public void draw(PApplet pa) {
		start.draw(pa);
		end.draw(pa);
		for (int i=0;i<block.length;i++)
			for (int j=0;j<block[0].length;j++)
				if (block[i][j]!=null) block[i][j].draw(pa);
	}

	public void createBug(Vector bugs) {
		bugs.add(new Bug(start.cenX(),start.cenY()));
	}

	public void marryBugs(Vector bugs) {
		
		for (int i=0;i<bugs.size();i++) {
			Bug b = (Bug)bugs.elementAt(i);
			if (end.in(b.x, b.y)) {
				if (!inEnd.contains(b)) {
					inEnd.add(b);
					b.stop();
					//bugs.removeElement(b);
					//i--;
				}
			}
		}
		
		while (inEnd.size()>=2) {
			// marry!
			//System.out.println("married: ");
			int i = (int)(inEnd.size()*Math.random());
			int j=i;
			while (i==j) j=(int)(inEnd.size()*Math.random());
			Bug b1 = (Bug)inEnd.elementAt(i);
			Bug b2 = (Bug)inEnd.elementAt(j);
			//System.out.println(b1.toString());
			//System.out.println(b2.toString());
			// MARRY:
			//System.out.println("new kids:");
			int nrOfChildren = (int)(b1.nrOfChildren + (Math.random()*(b2.nrOfChildren-b1.nrOfChildren)));
			for (int c=0;c<nrOfChildren;c++) {
				if (bugs.size()<MAX_BUGS) {
					Bug child = Bug.createChild(start,b1,b2);
					bugs.add(child);
					//System.out.println(child.toString());
				} else {
					//System.out.println("max reachged ("+bugs.size()+")");
				}
			}
			bugs.removeElement(b1);
			bugs.removeElement(b2);
			inEnd.removeElement(b1);
			inEnd.removeElement(b2);
		}
	}

	
}
