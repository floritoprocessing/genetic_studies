package graf.labyrinth;

import processing.core.PApplet;

public class Block {

	private int size;
	public int left, top, right, bottom;
	private int color = 0x808080;
	
	public Block(int size, int i, int j) {
		this.size = size;
		left=i*size;
		top=j*size;
		right = left+size;
		bottom = top+size;
		//System.out.println(i+" "+j);
	}
	
	public Block(int size, int i, int j, int color) {
		this(size,i,j);
		this.color = color;
	}

	public void draw(PApplet pa) {
		pa.rectMode(PApplet.CORNER);
		pa.stroke(255);
		pa.fill(0xFF<<24|color);
		pa.rect(left,top,size,size);
	}

	public boolean onTopOf(Block block) {
		if (block.left==this.left&&block.top==this.top) return true;
		return false;
	}

	public int cenX() {
		return left+size/2;
	}

	public int cenY() {
		return top+size/2;
	}

	public boolean horizontalIn(float x) {
		return (x>=left&&x<=right);
	}

	public boolean verticalIn(float y) {
		return (y>=top&&y<=bottom);
	}
	
	public boolean in(float x, float y) {
		return horizontalIn(x)&&verticalIn(y);
	}

}
