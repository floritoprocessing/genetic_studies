import graf.genetics.GeneMap;
import graf.genetics.Population;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;

import processing.core.PApplet;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class GeneticTryOut extends PApplet {
	
	public static void main(String[] args) {
		PApplet.main(new String[] {"GeneticTryOut"});
	}
	
	public static boolean DEBUG;
	
	static int LIFE_FRAMES = 150;
	static Point src = new Point(50,300);
	static Point dst = new Point(550,400);
	
	Population<Dingy> population = new Population<Dingy>();
	GeneMap<Dingy> geneMap;
	int gameOfLifeFrame = 0;

	public void settings() {
		size(600,600,P3D);
	}
	
	public void setup() {
		geneMap = new GeneMap<Dingy>(new Dingy(this));
		geneMap.registerFieldAsGene("direction");
		geneMap.registerFieldAsGene("floatArr");
		
		for (int i=0;i<300;i++) {
			Dingy animal = new Dingy(this);
			geneMap.createRandomDNAAndSetFields(animal);
			population.add(animal);
		}
	}
	
	public void draw() {
		background(255);
		
		ArrayList<Dingy> all = population.getMembers();
		
		if (gameOfLifeFrame==0) {
			for (Dingy animal:all) {
				animal.setPos(src.x, src.y);
			}
		} else {
			for (Dingy animal:all) {
				animal.move(this, dst);
			}
		}
		gameOfLifeFrame++;
		
		if (gameOfLifeFrame==LIFE_FRAMES) {
			gameOfLifeFrame=0;
			
			/*
			 * Evaluate fitness
			 */
			for (Dingy animal:all) {
				float distanceToTarget = dist(dst.x, dst.y, animal.x, animal.y);
				animal.setFitness(-distanceToTarget);
			}
			Collections.sort(all);
			for (int i=0;i<5;i++) {
				System.out.println(i+": "+all.get(i));
			}
			System.out.println("...");
			System.out.println((all.size()-1)+": "+all.get(all.size()-1));
			
			/*
			 * Breed next generation
			 */
			ArrayList<Dingy> nextGen = new ArrayList<Dingy>();
			for (int i=0;i<all.size();i++) {
				double rnd = Math.random(); // 0 .. 0.10 .. 0.25  .. 0.5 .. 1
				rnd = rnd*rnd; 				// 0 .. 0.01 .. 0.125 .. 0.25 .. 1 
				int papaIndex = (int)(rnd*all.size());
				int mamaIndex = papaIndex;
				while (mamaIndex==papaIndex) {
					rnd = Math.random();
					rnd = rnd*rnd;
					mamaIndex = (int)(rnd*all.size());
				}
				//System.out.println("Creating new animal");
				Dingy newAnimal = new Dingy(this);
				geneMap.crossBreedAndMutate(5,0.01f,all.get(mamaIndex), all.get(papaIndex), newAnimal);
				nextGen.add(newAnimal);
			}
			
			/*
			 * Replace old by new generation
			 */
			population.setMembers(nextGen);
		}
		
		
		
		/*
		 * draw
		 */
		rectMode(RADIUS);
		stroke(255,0,0);
		noFill();
		rect(src.x,src.y,10,10);
		rect(dst.x,dst.y,10,10);
		
		noStroke();
		fill(0,24);
		for (Dingy animal:all) {
			rect(animal.getX(), animal.getY(), 2, 2);
		}
		
	}
	
}
