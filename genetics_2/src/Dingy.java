import java.awt.Point;

import processing.core.PApplet;
import graf.genetics.Animal;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class Dingy extends Animal {
	
	//public byte speed;
	public int direction;
	public float[] floatArr = new float[60*60];
	
	float x, y;
	boolean dead = false;
	int frameDelay = 0;
	
	public Dingy(PApplet pa) {
		frameDelay = (int)pa.random(0,20);
	}
	
	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
	
	public void move(PApplet pa, Point destination) {
		
		if (!dead) {
			if (frameDelay>0) {
				frameDelay--;
				return;
			}
			double dir = (direction/(double)Integer.MAX_VALUE) * Math.PI;
			//double spd = Math.abs(speed/10.0f);
			float xm = (float)(5*Math.cos(dir));
			float ym = (float)(5*Math.sin(dir));
			x += xm;
			y += ym;
			if (x<0 || x>=pa.width || y<0 || y>=pa.height) {
				dead = true;
			}
			if (PApplet.dist(destination.x,destination.y,x,y)<10) {
				dead = true;
			}
		}
		
	}
	
	/**
	 * @param random
	 * @param random2
	 */
	public void setPos(float x, float y) {
		this.x=x;
		this.y=y;
	}

	@Override
	public String toString() {
		return super.toString()+", Dingy(), direction: "+direction;
	}

	

	
	
	
}
