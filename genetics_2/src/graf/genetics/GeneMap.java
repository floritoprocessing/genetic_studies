/**
 * 
 */
package graf.genetics;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Marcus
 * The GeneMap is responsible for managing the Gene/Field relationship.
 * It can turn read Genes and set Fields accordingly.
 * 
 */
public class GeneMap<E extends Animal> {
	
	public static boolean DEBUG = false;

	E exampleAnimal;
	Field[] animalFields = null;
	ArrayList<Gene> genes = new ArrayList<Gene>();
	ArrayList<Field> fields = new ArrayList<Field>();
	
	public GeneMap(E animal) {
		exampleAnimal = animal;
		animalFields = animal.getClass().getFields();
	}
	
	/**
	 * Registers a Field to a Gene. 
	 * When registering array fields, make sure that their size is predefined in the Animal.
	 * @param fieldName
	 */
	public void registerFieldAsGene(String fieldName) {
		for (Field field:animalFields) {
			if (DEBUG) {
				System.out.println(field.getName());
			}
			if (field.getName().equals(fieldName)) {
				Gene gene = createGene(field);
				if (gene!=null) {
					if (fields.contains(field)) {
						System.err.println("Field \""+fieldName+"\" already registered. Ignoring.");
					} else {
						if (gene.isArray() && !gene.hasLegalData()) {
							throw new RuntimeException("Your field \""+fieldName+"\" points to an array " +
									"which size has not been defined in the Animal");
						}
						genes.add(gene);
						fields.add(field);
						//if (DEBUG) {
							System.out.println("Registered \""+fieldName+"\" as "+gene);
						//}
						
					}
					return;
				} else {
					throw new RuntimeException("Could not register field \""+fieldName+"\". " +
							"A definition for the fieldType "+field.getType()+" is not present. See GeneMap.createGene()");
				}
			}
		}
		throw new RuntimeException("Could not find the field \""+fieldName+"\" in the defined Animal");
	}

	/**
	 * @param field
	 * @return
	 */
	private Gene createGene(Field field) {
		Class<?> fieldType = field.getType();
		
		if (fieldType.isArray()) {
			Class<?> componentType = fieldType.getComponentType();
			if (componentType.equals(byte.class)){
				//type = GeneType.ByteArray;
				return new Gene(Gene.Type.PrimByteArray, 
						RawConversion.toByta((byte[])getFieldValue(exampleAnimal, field)));
			}
			else if (componentType.equals(short.class)){
				//type = GeneType.IntArray;
				return new Gene(Gene.Type.PrimShortArray, 
						RawConversion.toByta((short[])getFieldValue(exampleAnimal, field)));
			}
			else if (componentType.equals(int.class)){
				//type = GeneType.IntArray;
				return new Gene(Gene.Type.PrimIntArray, 
						RawConversion.toByta((int[])getFieldValue(exampleAnimal, field)));
			}
			else if (componentType.equals(float.class)){
				//type = GeneType.FloatArray;
				return new Gene(Gene.Type.PrimFloatArray, 
						RawConversion.toByta((float[])getFieldValue(exampleAnimal, field)));
			}			
		} else {
			if (fieldType.equals(byte.class)){
				//type = GeneType.Byte;
				return new Gene(Gene.Type.PrimByte, 
						RawConversion.toByta((Byte)getFieldValue(exampleAnimal, field)));
			}
			else if (fieldType.equals(short.class)){
				//type = GeneType.Byte;
				return new Gene(Gene.Type.PrimShort, 
						RawConversion.toByta((Short)getFieldValue(exampleAnimal, field)));
			}
			else if (fieldType.equals(int.class)){
				//type = GeneType.Int;
				return new Gene(Gene.Type.PrimInt, 
						RawConversion.toByta((Integer)getFieldValue(exampleAnimal, field)));
			}
			else if (fieldType.equals(float.class)){
				//type = GeneType.Float;
				return new Gene(Gene.Type.PrimFloat, 
						RawConversion.toByta((Float)getFieldValue(exampleAnimal, field)));
			}
		}
		
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param exampleAnimal2
	 * @param field
	 * @return
	 */
	private Object getFieldValue(E animal, Field field) {
		field.setAccessible(true);
		try {
			Object value = field.get(animal);
			return value;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null; 
	}

	/**
	 * @param animal
	 */
	public void createRandomDNAAndSetFields(E animal) {
		DNA dna = new DNA();
		for (int i=0;i<genes.size();i++) {
			Gene gene = genes.get(i);
			Field field = fields.get(i);
			Gene newGene = (Gene)gene.clone();
			newGene.randomize();
			dna.addGene(newGene);
			setFieldValueFromGene(animal, field, newGene);
		}
		animal.setDNA(dna);
	}

	/**
	 * @param newGene
	 */
	private void setFieldValueFromGene(E animal, Field field, Gene gene) {
		field.setAccessible(true);
		Object value = gene.getValue();
		try {
			field.set(animal, value);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param crossAmount
	 * @param mutationChance
	 * @param dingy
	 * @param dingy2
	 * @param newAnimal
	 */
	public void crossBreedAndMutate(int crossAmount, float mutationChance, 
			E mother, E father, E child) {
		
		byte[] motherRaw = mother.dna.getRaw();
		byte[] fatherRaw = father.dna.getRaw();
		byte[] childRaw = new byte[motherRaw.length];
		
		for (Gene gene:mother.dna.genes){
			child.dna.addGene((Gene)(gene.clone()));
		}
		
		/*
		 * cross & mutate
		 */
		int[] crossings = new int[crossAmount];
		for (int i=0;i<crossings.length;i++) {
			crossings[i] = (int)(Math.random()*motherRaw.length);
		}
		Arrays.sort(crossings);
		
		//System.out.println("Creating raw");
		int source = (int)(Math.random()*2); // father or mother
		int crossPointIndex = 0;
		for (int i=0;i<motherRaw.length;i++) {
			if (crossPointIndex<crossings.length && crossings[crossPointIndex]==i) {
				source = 1-source;
				crossPointIndex++;
			}
			childRaw[i] = (source==0)?motherRaw[i]:fatherRaw[i];
			
			if (Math.random()<mutationChance) {
				childRaw[i] = (byte)(256*Math.random());
			}
		}
		
		/*
		 * Set raw data
		 */
		child.dna.setRaw(childRaw);
		
		/*
		 * Update values
		 */
		for (int i=0;i<genes.size();i++) {
			Gene gene = child.dna.genes.get(i);
			Field field = fields.get(i);
			setFieldValueFromGene(child, field, gene);
		}
		
		
	}

	
}
