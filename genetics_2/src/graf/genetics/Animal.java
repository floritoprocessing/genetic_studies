/**
 * 
 */
package graf.genetics;

/**
 * @author Marcus
 *
 */
public abstract class Animal implements Comparable<Animal> {
	
	public static boolean DEBUG;

	DNA dna = new DNA();
	private float fitness;
	
	public Animal() {
	}
	
	void setDNA(DNA dna) {
		this.dna = dna;
	}

	@Override
	public int compareTo(Animal o) {
		if (fitness<o.fitness) {
			return 1;
		} else if (fitness>o.fitness) {
			return -1;
		}
		return 0;
	}
	
	/**
	 * @param f
	 */
	public void setFitness(float f) {
		fitness = f;
	}
	
	public float getFitness() {
		return fitness;
	}

	@Override
	public String toString() {
		return "Animal, DNA length: "+dna.getSize()+", fitness: "+fitness;
	}
	
	
	
	
	
}
