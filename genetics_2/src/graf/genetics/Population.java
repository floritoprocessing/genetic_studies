/**
 * 
 */
package graf.genetics;

import java.util.ArrayList;

/**
 * @author Marcus
 *
 */
public class Population<E extends Animal> {
	
	public static boolean DEBUG;
	
	private ArrayList<E> animals = new ArrayList<E>();

	public Population() {
	}

	/**
	 * @return
	 */
	public ArrayList<E> getMembers() {
		return animals;
	}
	
	public void setMembers(ArrayList<E> animals) {
		this.animals = animals;
	}

	/**
	 * @param animal
	 */
	public void add(E animal) {
		animals.add(animal);
	}
}
