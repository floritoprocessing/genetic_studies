/**
 * 
 */
package graf.genetics;

import javax.management.RuntimeErrorException;

/**
 * @author Marcus
 * The Gene is the raw data representation holding an array of bytes.
 */
public class Gene implements Cloneable {
	
	public static boolean DEBUG;
	
	public enum Type {
		PrimByte(false),
		PrimInt(false),
		PrimShort(false),
		PrimFloat(false),
		
		PrimByteArray(true),
		PrimShortArray(true),
		PrimIntArray(true),
		PrimFloatArray(true);
		
		boolean array = false;
		
		Type(boolean array) {
			this.array = array;
		}
		
		boolean isArray() {
			return array;
		}
		
	}
	
	private Type type = null;
	private byte[] data = null;
	private boolean dataLegal = false;
	private boolean array = false;
	
	private Gene() {
	}
	
	public Gene(Type type, byte[] data) {
		this.type = type;
		this.data = data;
		dataLegal = (data!=null);
	}

	@Override
	public String toString() {
		return "Gene("+type+"), bytes: "+data.length;
	}
	
	@Override
	public Object clone() {
		Gene gene = new Gene();
		gene.type = type;
		gene.data = new byte[data.length];
		System.arraycopy(data, 0, gene.data, 0, data.length);
		gene.dataLegal = dataLegal;
		gene.array = array;
		return gene;
	}

	/**
	 * @return
	 */
	public boolean isArray() {
		return type.isArray();
	}

	/**
	 * @return
	 */
	public boolean hasLegalData() {
		return dataLegal;
	}

	/**
	 * 
	 */
	public void randomize() {
		for (int i=0;i<data.length;i++) {
			data[i] = (byte)(256*Math.random());
		}
	}

	/**
	 * @return the Gene represented as primitive or array
	 */
	public Object getValue() {
		Object out = null;
		switch (type) {
		case PrimByte:
			out = RawConversion.toByte(data);
			break;
		case PrimShort:
			out = RawConversion.toShort(data);
			break;
		case PrimInt:
			out = RawConversion.toInt(data);
			break;
		case PrimFloat:
			out = RawConversion.toFloat(data);
			break;
		case PrimByteArray:
			out = RawConversion.toByteA(data);
			break;
		case PrimShortArray:
			out = RawConversion.toByteA(data);
			break;
		case PrimIntArray:
			out = RawConversion.toIntA(data);
			break;
		case PrimFloatArray:
			out = RawConversion.toFloatA(data);
			break;
		default:
			throw new RuntimeException("Unknown type "+type);
		}
		
		return out;
	}
	
	void setRaw(byte[] rawData) {
		if (rawData.length!=data.length) {
			throw new RuntimeException("Illegal raw data for Gene");
		} else {
			System.arraycopy(rawData, 0, data, 0, data.length);
		}
	}
	
	byte[] getRaw() {
		return data;
	}

	/**
	 * @return
	 */
	public int size() {
		return data.length;
	}
	
	
}
