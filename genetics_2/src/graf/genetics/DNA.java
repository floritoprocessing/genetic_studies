/**
 * 
 */
package graf.genetics;

import java.util.ArrayList;

/**
 * @author Marcus
 * DNA is a collection of Genes. Genes can be read and converted to Fields by the GeneMap
 */
public class DNA {
	
	public static boolean DEBUG;
	
	ArrayList<Gene> genes = new ArrayList<Gene>();

	public DNA() {
	}

	/**
	 * @param newGene
	 */
	void addGene(Gene gene) {
		genes.add(gene);
	}

	public byte[] getRaw() {
		int size = 0;
		for (Gene gene:genes) {
			size += gene.size();
		}
		
		byte[] out = new byte[size];
		int offset = 0;
		for (Gene gene:genes) {
			System.arraycopy(gene.getRaw(), 0, out, offset, gene.size());
			offset += gene.size();
		}
		return out;
	}
	
	/**
	 * @return
	 */
	public int getSize() {
		int size = 0;
		for (Gene gene:genes) {
			size += gene.size();
		}
		return size;
	}

	/**
	 * @param raw
	 */
	public void setRaw(byte[] raw) {
		int[] sizes = new int[genes.size()];
		for (int i=0;i<genes.size();i++) {
			sizes[i] = genes.get(i).size();
		}
		int srcPos=0;
		for (int i=0;i<genes.size();i++) {
			byte[] subRaw = new byte[sizes[i]];
			System.arraycopy(raw, srcPos, subRaw, 0, subRaw.length);
			srcPos += subRaw.length;
			genes.get(i).setRaw(subRaw);
		}
	}
}
